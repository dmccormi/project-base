package controllers;

/**
 * Created by davidmccormick on 21/08/16.
 */

import akka.actor.*;
import model.TweetHub;
import model.TweetListener;

public class TweetWebSocketActor extends UntypedActor {

    /**
     * We don't create the actor ourselves. Instead, Play will ask Akka to make it for us. We have to give Akka a
     * "Props" object that tells Akka what kind of actor to create, and what constructor arguments to pass to it.
     * This method produces that Props object.
     */
    public static Props props(String topic, ActorRef out) {
        // Create a Props object that says:
        // - I want a TweetWebSocketActor,
        // - and pass (topic, out) as the arguments to its constructor
        return Props.create(TweetWebSocketActor.class, topic, out);
    }

    /** The Actor for the client (browser) */
    private final ActorRef out;

    /** The topic string we have subscribed to */
    private String topic;

    /** A listener that we will register with our TweetHub */
    private final TweetListener listener;

    /**
     * This constructor is called by Akka to create our actor (we don't call it ourselves).
     */
    public TweetWebSocketActor(String topic, ActorRef out) {
        this.topic = topic;
        this.out = out;

        /*
          Our TweetListener, written as a Java 8 Lambda.
          Whenever we receive a tweet, if it matches our topic, convert it to a JSON string, and send it to the client.
         */
        this.listener = (tweetJson) -> {
            if (!tweetJson.findValue("msg").toString().matches(String.format("(?i).*%s.*", this.topic)) ){
                return;
            }

            String message = tweetJson.toString();

            out.tell(message, self());
        };

        // Register this actor to hear tweets
        TweetHub.getInstance().addListener(listener);
    }

    /**
     * This is called whenever the browser sends a message to the serverover the websocket
     */
    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            String newTopic = message.toString();
            if (message.toString().isEmpty()){ newTopic = ".*";}
            this.topic = newTopic;
        }
    }

    /**
     * This is called by Play after the WebSocket has closed
     */
    public void postStop() throws Exception {
        // De-register our listener
        TweetHub.getInstance().removeListener(this.listener);
    }
}
