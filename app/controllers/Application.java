package controllers;

import actors.Setup;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import model.UserService;
import play.mvc.Controller;
import play.mvc.LegacyWebSocket;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.compat.java8.FutureConverters;
import play.libs.ws.*;

import java.util.concurrent.CompletionStage;
import static akka.pattern.Patterns.ask;
import static controllers.UserController.getSessionId;
import static controllers.UserController.getUserService;

@Singleton
public class Application extends Controller {

    Setup actorSetup;

    WSClient wsClient;

    protected static UserService getUserService() {
        return UserService.instance;
    }

    protected static final String SESSIONVAR = "mySession";

    /**
     * Returns our generated session ID for this user, creating one if necessary
     */
    protected static String getSessionId() {
        String id = session(SESSIONVAR);
        if (!getUserService().isValidId(id)) {
            id = getUserService().allocateId();
            session(SESSIONVAR, id);
        }
        return id;
    }

    @Inject
    public Application(Setup actorSetup, WSClient wsClient) {
        this.actorSetup = actorSetup;
        this.wsClient = wsClient;
    }

    public Result index() {
        return ok(views.html.application.index.render(getUserService().getUserFromSession(getSessionId())));
    }

    /**
     * Our WebSockets endpoint. We'll assume we're sending String messages for now
     */
    public LegacyWebSocket<String> socket(String topic) {

        /*
         Play framework provides an Actor for client automatically. That's the <code>out</code> argument below.

         We need to tell Play what kind of actor we want on the server side. We do that with a "Props" object, because
         Play will ask Akka to create the actor for us.

         We want a TweetWebSocketActor, and we want to pass the client actor and the topic as constructor arguments.
         TweetWebSocketActor.props(topic, out) will produce a Props object saying that.
         */
        return WebSocket.withActor((out) -> TweetWebSocketActor.props(topic, out));
    }
}
