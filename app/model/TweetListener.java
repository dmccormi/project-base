package model;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Created by davidmccormick on 21/08/16.
 */
public interface TweetListener {

    void receiveTweet(ObjectNode g);

}