package model;

/**
 * Created by davidmccormick on 21/08/16.
 */

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is a simple publish-subscribe list. It maintains a list of listeners, and whenever it receives a call to
 * <code>send</code>, it calls <code>receiveTweet</code> on every registered listener.
 */
public class TweetHub {

    List<TweetListener> listeners;

    static final TweetHub instance = new TweetHub();

    public static TweetHub getInstance() {
        return instance;
    }

    protected TweetHub() {
        this.listeners = Collections.synchronizedList(new ArrayList<>());
    }

    public void send(ObjectNode g) {
        for (TweetListener listener : listeners) {
            listener.receiveTweet(g);
        }
    }

    public void addListener(TweetListener l) {
        this.listeners.add(l);
    }

    public void removeListener(TweetListener l) {
        this.listeners.remove(l);
    }

}