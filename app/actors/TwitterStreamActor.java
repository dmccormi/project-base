package actors;

import com.fasterxml.jackson.databind.node.ObjectNode;
import model.TweetHub;
import play.libs.Json;
import twitter4j.*;

import akka.actor.Props;
import akka.actor.UntypedActor;

import twitter4j.conf.ConfigurationBuilder;


/*
Consumer Key (API Key)	MkpBoVPy3REAIdsPNSZpC7u0I
Consumer Secret (API Secret)	3cNybn6qSJPpw2AnizXGitOclFV3ZolFXidWBWLpOnafRw7uL8
Access Token	315679785-SVT1kCKoow1eCTgTWLzPrh6BPaKA5OiPOfIlmAxt
Access Token Secret	TYTg0PLDbU7isjXxm3uPIeu2xdYg3rXGaEGUdJuBkvCg2
*/



/**
 * A rather trivial Actor that just plays FizzBuzz
 */
public class TwitterStreamActor extends UntypedActor {

    int nextNum = 0;

    /**
     *
     */
    public static Props props = Props.create(TwitterStreamActor.class);

    TwitterStream twitterStream;
    StatusListener listener;

    public TwitterStreamActor() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setHttpProxyHost("proxy.une.edu.au")
                .setHttpProxyPort(8080)
                .setHttpProxyUser("INSERT_USER")
                .setHttpProxyPassword("INSERT_PASS")
                .setOAuthConsumerKey("URXzqYknjyh5nmi3QI6foQk6j")
                .setOAuthConsumerSecret("orojeRnVyE1wedBeI9lGm3zttEIKb8m9YxYIiMi7PF3iXvpIEb")
                .setOAuthAccessToken("315679785-XmK78wA8vCNfMgLA2S98svB4lWAXbKWkzILK6hxJ")
                .setOAuthAccessTokenSecret("2Eztppqloh2h8xQhBscsASPsWMjTKzvsaw0LnO0HEv92R");


        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

        listener = new StatusListener() {
            @Override
            public void onStatus(Status status) {

                // Work around for bug in filter.
                GeoLocation loc = status.getGeoLocation();
                if (loc == null){
                    return;
                }

                // Build a json object to send over the wire to the client.
                ObjectNode n = Json.newObject();
                n.put("msg", status.getText());
                n.put("lat", loc.getLatitude());
                n.put("lng", loc.getLongitude());
                TweetHub.getInstance().send(n);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                return;
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                System.out.println("Got stall warning:" + warning);
            }

            @Override
            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };

    }

    @Override
    public void onReceive(Object message) throws TwitterException {
        if (message.equals("ConnectToStream")){
            // Effectively the wake up call that starts the stream up.
            ConnectToTwitterStreamWithFilter();
        }

    }

    void ConnectToTwitterStreamWithFilter(){
        FilterQuery fq = new FilterQuery();
        double[][] boundingBox= {{-180, -90}, {180, 90}}; // whole world;
        fq.locations(boundingBox);
        twitterStream.addListener(listener);
        twitterStream.filter(fq);
    }
}
