package actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Holds all the "famous" Actors in our system. This hopefully makes things convenient, because if you @Inject Setup,
 * you'll then be able to look up ActorRefs to all the actors just by Setup.marshallActor, etc.
 *
 * It's a Singleton, so Google Guice will only create one of these for us.
 */
@Singleton
public class Setup {

    @Inject
    public Setup(ActorSystem system) {
        ActorRef fb1 = system.actorOf(TwitterStreamActor.props, "TwitterStream");
        fb1.tell("ConnectToStream", ActorRef.noSender());
    }
}