var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var twitter = require("ntwitter");

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

var tw = new twitter({
        consumer_key: "YOUR_CONSUMER_KEY",
        consumer_secret: "YOUR_CONSUMER_SECRET",
        access_token_key: "USER_ACCESS_TOKEN",
        access_token_secret: "USER_ACCESS_TOKEN_SECRET"
    }),
    stream = null,
    track = "",
    users = [];

var iosa = io.of('/socket');
io.on('connection', function(socket, location) {

    tw.stream("statuses/filter", {
            track: track
        }, function(s) {
            stream = s;
            stream.on("data", function(data) {
                socket.emit('tweet', { lat: location.lat, lon: location.lng });
            });
    });

});

server.listen(8080);


import React, {PropTypes, Component} from 'react';
import shouldPureComponentUpdate from 'react-pure-render/function';

import GoogleMap from 'google-map-react';

  constructor(props) {
    super(props);
  }

  render() {
    return (
       <GoogleMap
        defaultCenter={this.props.center}
        defaultZoom={this.props.zoom}>
      </GoogleMap>
    );
  }
}